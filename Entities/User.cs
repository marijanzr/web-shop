using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.Design;

namespace web_shop.Entities
{
    public class User
    {
        public int UserId { get; set; }
        [Required(ErrorMessage = "Molimo unesite email adresu!")]
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }
    }
}