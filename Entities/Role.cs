using System.ComponentModel.DataAnnotations;
namespace web_shop.Entities
{
    public class Role
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
        public string RoleName { get; set; }
    }
}