using System;
using Microsoft.EntityFrameworkCore;
using FluentValidation;
using web_shop.Models;
using web_shop.Data;

namespace web_shop.Validators
{
    public class RegisterUserValidator : AbstractValidator<UserDtoCreate>
    {
        public RegisterUserValidator(ApplicationDbContext applicationDbContext)
        {
            RuleFor(x => x.Email).NotEmpty();
            RuleFor(x => x.Email).EmailAddress().WithMessage("Molimo unesite valjanu email adresu");
            RuleFor(x => x.Password).MinimumLength(6);
            RuleFor(x => x.Email).Custom(async (value, context) =>
            {
                var userAlredyExist = await applicationDbContext.Users.AnyAsync(user => user.Email == value);
                if (userAlredyExist)
                {
                    context.AddFailure("Email", "Ovaj email je već registriran");
                }
            });
            RuleFor(x => x.RoleId).Custom(async (value, context) =>
            {
                var roleExist = await applicationDbContext.Roles.AnyAsync(role => role.RoleId == value);
                if (!roleExist)
                {
                    context.AddFailure("Rola ne postoji");
                }
            });
        }
    }
}