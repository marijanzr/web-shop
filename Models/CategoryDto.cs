
namespace web_shop.Models
{
    public class CategoryDto
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }

    public class CategoryDtoCreate
    {
        public string CategoryName { get; set; }
    }
}