namespace web_shop.Models
{
    public class RoleDto
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
        public string RoleName { get; set; }

    }


    public class RoleDtoCreate
    {

        public string Name { get; set; }
        public string RoleName { get; set; }

    }
}