using System.ComponentModel.DataAnnotations.Schema;


namespace web_shop.Models
{
    public class UserDto
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
    public class UserDtoCreate
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }

    }

    public class UserDtoLogin
    {
        public string Email { get; set; }
        public string Password { get; set; }

    }
}