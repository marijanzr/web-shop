
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using web_shop.Data;
using web_shop.Models;
using web_shop.Entities;
using web_shop.Identity;

namespace web_shop
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {

        private readonly ApplicationDbContext _db;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IJwtProvider _jwtProvider;

        public UsersController(ApplicationDbContext db, IPasswordHasher<User> passwordHasher, IJwtProvider jwtProvider)
        {
            _db = db;
            _passwordHasher = passwordHasher;
            _jwtProvider = jwtProvider;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<UserDto>>> GetUsers()
        {

            var userDto = await _db.Users.Select(x => new UserDto
            {
                UserId = x.UserId,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email
            }).ToListAsync();


            return userDto;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> GetUser(int id)
        {
            var user = await _db.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }
            else
            {
                var userDto = new UserDto
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName
                };
                return Ok(userDto);
            }

        }

        // [HttpPost]
        // public async Task<ActionResult<UserDtoCreate>> AddUser(UserDtoCreate userDtoCreate)
        // {
        //     var newUser = new User
        //     {
        //         RoleId = userDtoCreate.RoleId,
        //         FirstName = userDtoCreate.FirstName,
        //         LastName = userDtoCreate.LastName,
        //         Email = userDtoCreate.Email,
        //         PasswordHash = userDtoCreate.PasswordHash

        //     };
        //     await _db.Users.AddAsync(newUser);
        //     await _db.SaveChangesAsync();
        //     return CreatedAtAction("User", new { id = newUser.UserId }, newUser);
        // }


        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteUser(int userId)
        {
            var user = await _db.Users.FindAsync(userId);
            if (user == null)
            {
                return NotFound();
            }
            _db.Users.Remove(user);
            await _db.SaveChangesAsync();
            return NoContent();
        }

        [HttpPost("register")]
        public async Task<ActionResult<UserDtoCreate>> Register([FromBody] UserDtoCreate userDtoCreate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(userDtoCreate);
            }
            var newUser = new User
            {
                FirstName = userDtoCreate.FirstName,
                LastName = userDtoCreate.LastName,
                Email = userDtoCreate.Email,
                RoleId = 2,
            };

            var passwordHash = _passwordHasher.HashPassword(newUser, userDtoCreate.Password);
            newUser.Password = passwordHash;

            await _db.Users.AddAsync(newUser);
            await _db.SaveChangesAsync();
            return CreatedAtAction("User", new { UserId = newUser.UserId }, newUser);
        }

        [HttpPost("login")]
        public async Task<ActionResult> Login([FromBody] UserDtoLogin userDtoLogin)
        {
            var user = await _db.Users.Include(user => user.Role)
            .FirstOrDefaultAsync(user => user.Email == userDtoLogin.Email);
            if (user == null)
            {
                return BadRequest("Pogrešno korisničko ime ili lozinka");
            }
            var passwordVerificationResult = _passwordHasher.VerifyHashedPassword(user, user.Password, userDtoLogin.Password);
            if (passwordVerificationResult == PasswordVerificationResult.Failed)
            {
                return BadRequest("Pogrešno korisničko ime ili lozinka");

            }
            var token = _jwtProvider.GenerateJwtToken(user);
            return Ok(token);
        }

    }
}