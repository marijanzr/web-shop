using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using web_shop.Data;
using web_shop.Models;
using web_shop.Entities;
using Microsoft.EntityFrameworkCore;

namespace web_shop
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : Controller
    {
        private readonly ApplicationDbContext _db;

        public CategoryController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryDto>>> GetCategories()
        {

            var category = await _db.Category.Select(x => new CategoryDto
            {
                CategoryId = x.CategoryId,
                CategoryName = x.CategoryName,
            }).ToListAsync();

            return category;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDto>> GetCategory(int id)
        {
            var category = await _db.Category.FindAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            var categoryDto = new CategoryDto
            {
                CategoryId = category.CategoryId,
                CategoryName = category.CategoryName
            };

            return categoryDto;
        }

        [HttpPost]
        public async Task<ActionResult<CategoryDtoCreate>> AddCategory(CategoryDtoCreate categoryDtoCreate)
        {

            var category = new Category
            {
                CategoryName = categoryDtoCreate.CategoryName
            };

            await _db.Category.AddAsync(category);
            await _db.SaveChangesAsync();
            return CreatedAtAction("Category", new { id = category.CategoryId }, category);
        }

        // [HttpPut("{id}")]
        // public async Task<IActionResult> UpdateCategory(int id, Category category)
        // {
        //     if (id != category.CategoryId)
        //     {
        //         return BadRequest();
        //     }
        //     _db.Entry(category).State = EntityState.Modified;
        //     try
        //     {
        //         await _db.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {

        //         throw;

        //     }
        //     return NoContent();
        // }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            var category = await _db.Category.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            _db.Category.Remove(category);
            await _db.SaveChangesAsync();

            return NoContent();
        }

    }
}