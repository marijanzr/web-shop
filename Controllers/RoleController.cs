using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using web_shop.Models;
using web_shop.Entities;
using web_shop.Data;

namespace web_shop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {

        private readonly ApplicationDbContext _db;
        public RoleController(ApplicationDbContext db)
        {
            _db = db;
        }
        // GET: api/Role
        [HttpGet]

        public async Task<ActionResult<IEnumerable<RoleDto>>> GetRoles()
        {

            var roleDto = await _db.Roles.Select(x => new RoleDto
            {
                RoleId = x.RoleId,
                Name = x.Name,
                RoleName = x.RoleName
            }).ToListAsync();


            return roleDto;
        }

        // POST: api/Role
        [HttpPost]
        public async Task<ActionResult<Role>> PostRole([FromBody] RoleDtoCreate roleDtoCreate)
        {
            var newRole = new Role
            {
                Name = roleDtoCreate.Name,
                RoleName = roleDtoCreate.RoleName
            };

            await _db.Roles.AddAsync(newRole);
            await _db.SaveChangesAsync();
            return CreatedAtAction("Role", new { roleId = newRole.RoleId }, newRole);

        }

        [HttpDelete("{roleId}")]
        public async Task<ActionResult> DeleteRole(int roleId)
        {
            var role = await _db.Roles.FindAsync(roleId);
            if (role == null)
            {
                return NotFound();
            }
            _db.Roles.Remove(role);
            await _db.SaveChangesAsync();
            return NoContent();
        }
    }
}
