using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using web_shop.Data;
using web_shop.Models;
using web_shop.Entities;
using Microsoft.EntityFrameworkCore;

namespace web_shop
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : Controller
    {
        private readonly ApplicationDbContext _db;

        public ProductsController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductDto>>> GetProducts()
        {

            return await _db.Products.Select(x => new ProductDto
            {
                ProductId = x.ProductId,
                Title = x.Title,
                Description = x.Description,
                ImageUrl = x.ImageUrl,
                Price = x.Price
            }).ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductDto>> GetProduct(int id)
        {
            var product = await _db.Products.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            var productDto = new ProductDto { ProductId = product.ProductId, Title = product.Title };

            return productDto;
        }

        [HttpPost]
        public async Task<ActionResult<ProductDtoCreate>> AddProduct(ProductDtoCreate productDto)
        {
            var product = new Product
            {
                CategoryId = productDto.CategoryId,
                Title = productDto.Title,
                Description = productDto.Description,
                ImageUrl = productDto.ImageUrl,
                Price = productDto.Price
            };
            await _db.Products.AddAsync(product);
            await _db.SaveChangesAsync();
            return CreatedAtAction("Product", new { id = product.ProductId }, product);
        }

        // [HttpPut("{id}")]
        // public async Task<IActionResult> UpdateProduct(int id, Product product)
        // {
        //     if (id != product.ProductId)
        //     {
        //         return BadRequest();
        //     }
        //     _db.Entry(product).State = EntityState.Modified;
        //     try
        //     {
        //         await _db.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {

        //         throw;

        //     }
        //     return NoContent();
        // }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            var product = await _db.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _db.Products.Remove(product);
            await _db.SaveChangesAsync();

            return NoContent();
        }

    }
}