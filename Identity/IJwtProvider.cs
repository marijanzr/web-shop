using web_shop.Entities;
namespace web_shop.Identity
{
    public interface IJwtProvider
    {
        string GenerateJwtToken(User user);
    }
}