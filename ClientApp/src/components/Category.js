import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Select from "@material-ui/core/Select";

import { getCategory } from "../redux/categorySlice";
import { filterProductByCategory } from "../redux/productSlice";

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const Category = ({ open, setOpen }) => {
  const { category } = useSelector((state) => state.category);
  const [selectedCategory, setSelectedCategory] = useState();
  const [categoryId, setCategoryId] = useState();

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCategory());
  }, [dispatch]);

  const handleClose = () => {
    setOpen(false);
  };

  const handleSelectCategory = (e) => {
    setSelectedCategory(e.target.value);
    setCategoryId(e.target.value);
  };

  const handleSelectedCategory = (e) => {
    setOpen(false);
    dispatch(filterProductByCategory(categoryId));
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Odaberite kategoriju
        </DialogTitle>
        <DialogContent dividers>
          <Select
            native
            value={selectedCategory}
            name="category"
            onChange={handleSelectCategory}
          >
            <option value={-1}>Sve kategorije</option>
            {category.map((c, i) => (
              <option value={c.categoryId} key={i}>
                {c.categoryName}
              </option>
            ))}
          </Select>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleSelectedCategory} color="primary">
            Pretraži
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default Category;
