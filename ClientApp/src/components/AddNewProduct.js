import React from "react";
import { useSelector } from "react-redux";

import { makeStyles, withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";

import { createProduct } from "../redux/indexApi";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;

  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const AddNewProduct = () => {
  const { category } = useSelector((state) => state.category);
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const [product, setProduct] = React.useState({
    title: "",
    description: "",
    imageUrl: "",
    categoryId: "",
    price: "",
  });

  const handleChange = (prop) => (event) => {
    setProduct({ ...product, [prop]: event.target.value });
  };

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleNewProductSubmit = (e) => {
    e.preventDefault();
    createProduct(product);
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Dodaj novi proizvod
      </Button>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Dodaj novi proizvod
        </DialogTitle>
        <DialogContent dividers>
          <form onSubmit={handleNewProductSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="productName"
              label="Naziv proizvoda"
              name="productName"
              autoFocus
              onChange={handleChange("title")}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="productDescription"
              label="Opis Proizvoda"
              name="productDescription"
              autoFocus
              onChange={handleChange("description")}
            />

            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="price"
              label="Cijena"
              name="price"
              autoFocus
              type="number"
              onChange={handleChange("price")}
            />

            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel htmlFor="filled-age-native-simple">
                Kategorija
              </InputLabel>
              <Select
                native
                value={product.categoryId}
                name="category"
                onChange={handleChange("categoryId")}
                inputProps={{
                  name: "category",
                  id: "category",
                }}
              >
                {category.map((c, i) => (
                  <option value={c.categoryId} key={i}>
                    {c.categoryName}
                  </option>
                ))}
              </Select>
            </FormControl>

            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="imageUrl"
              label="Url slike"
              name="imageUrl"
              autoFocus
              onChange={handleChange("imageUrl")}
            />
          </form>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="secondary">
            Odustani
          </Button>
          <Button
            autoFocus
            onClick={handleNewProductSubmit}
            color="primary"
            type="submit"
          >
            Spremi
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default AddNewProduct;
