import React from "react";
import Grid from "@material-ui/core/Grid";
import ProductCard from "./ProductCard";

const ProductsGrid = ({ products }) => {
  return (
    <Grid container spacing={2}>
      {products.map((product, index) => (
        <Grid item key={index} xs={12} md={6} lg={3}>
          <ProductCard product={product} />
        </Grid>
      ))}
    </Grid>
  );
};

export default ProductsGrid;
