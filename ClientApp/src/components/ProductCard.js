import React, { useState } from "react";
import { Link } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";
import Button from "@material-ui/core/Button";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";

import { useDispatch } from "react-redux";
import {
  addProductInCart,
  incrementNumberOfProducts,
  removeProductFromCart,
  decrementNumberOfProducts,
  calculateTotalPrice,
  addProductToFavorite,
  removeProductFromFavorite,
} from "../redux/cartSlice";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  buttonBuyed: {
    backgroundColor: "green",
  },
}));

export default function ProductCard({ product }) {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [buyButtonText, setBuyButtonText] = useState("Dodaj");
  const [buyButtonIsBuyed, setBuyButtonIsBuyed] = useState(false);
  const [isFavorite, setIsFavorite] = useState(false);

  const handleIsBuyed = () => {
    if (!buyButtonIsBuyed) {
      setBuyButtonText("Dodano");
      dispatch(addProductInCart(product));
      dispatch(incrementNumberOfProducts());
      setBuyButtonIsBuyed(true);
      dispatch(calculateTotalPrice());
    } else {
      setBuyButtonText("Dodaj");
      dispatch(removeProductFromCart(product.productId));
      dispatch(decrementNumberOfProducts());
      setBuyButtonIsBuyed(false);
      dispatch(calculateTotalPrice());
    }
  };

  const handleFavoriteClick = () => {
    if (!isFavorite) {
      dispatch(addProductToFavorite(product));
      setIsFavorite(!isFavorite);
    } else {
      dispatch(removeProductFromFavorite(product.productId));
      setIsFavorite(!isFavorite);
    }
  };

  return (
    <Card className={classes.root}>
      <CardHeader title={product.title} subheader="September 14, 2016" />
      <CardMedia
        className={classes.media}
        image={
          product.imageUrl ||
          "https://images.unsplash.com/photo-1557821552-17105176677c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1189&q=80"
        }
        title={product.title}
      />
      <CardContent>
        <Typography color="textSecondary" component="p">
          {product.price} HRK
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {product.description}
        </Typography>
      </CardContent>
      <CardActions>
        <IconButton aria-label="add to favorites" onClick={handleFavoriteClick}>
          <FavoriteIcon style={isFavorite ? { fill: "red" } : { fill: "" }} />
        </IconButton>
        <Button
          variant="contained"
          color="secondary"
          className={buyButtonIsBuyed ? classes.buttonBuyed : classes.button}
          startIcon={<AddShoppingCartIcon />}
          onClick={handleIsBuyed}
        >
          {buyButtonText}
        </Button>
        <Link to={`products/${product.productId}`}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
          >
            Detalji
          </Button>
        </Link>
      </CardActions>
    </Card>
  );
}
