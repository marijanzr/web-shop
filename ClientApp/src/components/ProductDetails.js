import React from "react";
import { useSelector } from "react-redux";

import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    // maxWidth: 345,
  },
  media: {
    height: 400,
  },
});

const ProductDetails = (props) => {
  const classes = useStyles();
  const { products } = useSelector((state) => state.products);
  const product = products.filter(
    (product) => product.productId == props.match.params.id
  );

  return (
    <div>
      <Typography variant="h4">Detalji proizvoda</Typography>
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={
              product[0].imageUrl ||
              "https://data.labin.com/web/fotovijesti/vijesti_23251_v.jpg"
            }
            title={product[0].title}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {product[0].title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {product[0].description}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary">
            Share
          </Button>
          <Button size="small" color="primary">
            Learn More
          </Button>
        </CardActions>
      </Card>
    </div>
  );
};

export default ProductDetails;
