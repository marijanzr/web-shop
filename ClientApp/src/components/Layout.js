import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';

import  NavMenu  from "./NavMenu";

import React from "react";

function Layout(props) {
  return (
    <Box >
      <NavMenu />
      <Box mt={2} mb={2}>
      <Container disableGutters >{props.children}</Container>
      </Box>
    </Box>
  );
}

export default Layout;
