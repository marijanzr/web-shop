import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import ButtonBase from "@material-ui/core/ButtonBase";
import { useSelector, useDispatch } from "react-redux";
import { removeProductFromFavorite } from "../redux/cartSlice";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: "auto",
    maxWidth: 800,
    marginBottom: 8,
  },
  image: {
    width: 128,
    height: 128,
  },
  img: {
    margin: "auto",
    display: "block",
    maxWidth: "100%",
    maxHeight: "100%",
  },
}));

const Favorite = () => {
  const classes = useStyles();
  const { cart } = useSelector((state) => state.cart);
  const { favorite } = useSelector((state) => state.cart);
  const [cartButtonText, setCartButtonText] = useState("Dodaj u košaricu");
  const [isProductInCart, setIsProductInCart] = useState(false);

  const dispatch = useDispatch();

  // useEffect(() => {

  // }, []);

  const checkIfProductIsInCart = (cart) => {
    return cart.filter((product) => product.product.Id == product.productId);
  };

  const handleRemoveProduct = (productId) => {
    dispatch(removeProductFromFavorite(productId));
  };

  const handleAddRemoveFromCart = (product) => {};

  return (
    <div>
      {favorite.length > 0 ? (
        <div className={classes.root}>
          {favorite.map((product, index) => (
            <Paper className={classes.paper} key={index}>
              <Grid container spacing={2}>
                <Grid item>
                  <Link to={`products/${product.productId}`}>
                    <ButtonBase className={classes.image}>
                      <img
                        className={classes.img}
                        alt="complex"
                        src={
                          product.imageUrl ||
                          "https://images.unsplash.com/photo-1557821552-17105176677c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1189&q=80"
                        }
                      />
                    </ButtonBase>
                  </Link>
                </Grid>
                <Grid item xs={12} sm container>
                  <Grid item xs container direction="column" spacing={2}>
                    <Grid item xs>
                      <Typography gutterBottom variant="subtitle1">
                        {product.title}
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        {product.description}
                      </Typography>
                      <Typography variant="body2" color="textSecondary">
                        ID: 1030114
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography
                        variant="body2"
                        style={{ cursor: "pointer" }}
                        onClick={() => handleRemoveProduct(product.productId)}
                      >
                        Ukloni iz favorita
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography
                        variant="body2"
                        style={{ cursor: "pointer" }}
                        onClick={() => handleAddRemoveFromCart(product)}
                      >
                        {cartButtonText}
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid item>
                    <Typography variant="subtitle1">
                      {product.price} HRK
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          ))}
        </div>
      ) : (
        <div>
          <p>Nemate favorita</p>
        </div>
      )}
    </div>
  );
};
export default Favorite;
