import React from "react";
import { Link, useHistory } from "react-router-dom";
import { createUser } from "../redux/indexApi";

import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";

import Box from "@material-ui/core/Box";
import FormControl from "@material-ui/core/FormControl";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(20),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  margin: {
    margin: theme.spacing(1),
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: "35ch",
  },
}));

const Registration = () => {
  const classes = useStyles();
  const history = useHistory();
  const [values, setValues] = React.useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
  });

  const [showPassword, setShowPassword] = React.useState(false);

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleRegistration = (e) => {
    e.preventDefault();
    e.preventDefault();
    if (!values.email) {
      return console.log("Unesite email");
    }
    if (!values.password || values.password.length < 6) {
      return console.log("Unesite lozinku");
    }
    createUser(values);
    history.push("/");
  };
  return (
    <form className={classes.root}>
      <TextField
        label="Ime"
        id="outlined-start-adornment"
        className={clsx(classes.margin, classes.textField)}
        variant="outlined"
        onChange={handleChange("firstName")}
        required
      />

      <TextField
        label="Prezime"
        id="outlined-start-adornment"
        className={clsx(classes.margin, classes.textField)}
        variant="outlined"
        onChange={handleChange("lastName")}
        required
      />

      <TextField
        label="Email"
        id="outlined-start-adornment"
        type="email"
        className={clsx(classes.margin, classes.textField)}
        variant="outlined"
        onChange={handleChange("email")}
      />

      <FormControl
        className={clsx(classes.margin, classes.textField)}
        variant="outlined"
      >
        <InputLabel htmlFor="outlined-adornment-password">Lozinka</InputLabel>
        <OutlinedInput
          id="outlined-adornment-password"
          type={showPassword ? "text" : "password"}
          value={values.password}
          onChange={handleChange("password")}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                edge="end"
              >
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          }
          labelWidth={70}
        />
      </FormControl>
      <Box>
        <Button
          variant="contained"
          color="primary"
          onClick={handleRegistration}
          type="submit"
        >
          Registracija
        </Button>
      </Box>
      <Box>
        <Link to="/login">
          <Button color="primary">Prijava</Button>
        </Link>
      </Box>
    </form>
  );
};

export default Registration;
