import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import ProductsGrid from "../components/ProductsGrid";
import { getProducts } from "../redux/productSlice";

export default function Home() {
  const { filteredProducts } = useSelector((state) => state.products);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  return <ProductsGrid products={filteredProducts} />;
}
