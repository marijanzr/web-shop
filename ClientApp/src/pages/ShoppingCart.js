import React from "react";
import { Link } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";
import ButtonBase from "@material-ui/core/ButtonBase";
import { useSelector, useDispatch } from "react-redux";
import {
  removeProductFromCart,
  decrementNumberOfProducts,
  calculateTotalPrice,
} from "../redux/cartSlice";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: "auto",
    maxWidth: 800,
    marginBottom: 8,
  },
  image: {
    width: 128,
    height: 128,
  },
  img: {
    margin: "auto",
    display: "block",
    maxWidth: "100%",
    maxHeight: "100%",
  },
}));

export default function ShoppingCart() {
  const classes = useStyles();
  const { cart } = useSelector((state) => state.cart);
  const { totalPriceOfProducts } = useSelector((state) => state.cart);

  const dispatch = useDispatch();

  const handleRemoveProduct = (productId) => {
    dispatch(removeProductFromCart(productId));
    dispatch(decrementNumberOfProducts());
    dispatch(calculateTotalPrice());
  };

  return (
    <div>
      {cart.length > 0 ? (
        <div className={classes.root}>
          {cart.map((product, index) => (
            <Paper className={classes.paper} key={index}>
              <Grid container spacing={2}>
                <Grid item>
                  <Link to={`products/${product.productId}`}>
                    <ButtonBase className={classes.image}>
                      <img
                        className={classes.img}
                        alt="complex"
                        src={
                          product.imageUrl ||
                          "https://images.unsplash.com/photo-1557821552-17105176677c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1189&q=80"
                        }
                      />
                    </ButtonBase>
                  </Link>
                </Grid>
                <Grid item xs={12} sm container>
                  <Grid item xs container direction="column" spacing={2}>
                    <Grid item xs>
                      <Typography gutterBottom variant="subtitle1">
                        {product.title}
                      </Typography>
                      <Typography variant="body2" gutterBottom>
                        {product.description}
                      </Typography>
                      <Typography variant="body2" color="textSecondary">
                        ID: 1030114
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography
                        variant="body2"
                        style={{ cursor: "pointer" }}
                        onClick={() => handleRemoveProduct(product.productId)}
                      >
                        Ukloni iz košarice
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid item>
                    <Typography variant="subtitle1">
                      {product.price} HRK
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          ))}
          <Paper className={classes.paper}>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography>
                  Ukupna cijena: {totalPriceOfProducts} HRK
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Link to="index">
                  <Button size="small" color="primary">
                    Nastavi kupnju
                    <ArrowRightAltIcon />
                  </Button>
                </Link>
              </Grid>
            </Grid>
          </Paper>
        </div>
      ) : (
        <div>
          <p>Nema proizvoda u košarici</p>
        </div>
      )}
    </div>
  );
}
