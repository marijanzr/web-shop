import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { API } from "./indexApi";

export const getCategory = createAsyncThunk(
  "category/getCategory",
  async () => {
    return await fetch(`${API}/category`).then((res) => res.json());
  }
);

const categorySlice = createSlice({
  name: "category",
  initialState: {
    category: [],
    status: null,
  },
  extraReducers: {
    [getCategory.pending]: (state) => {
      state.action = "loading";
    },
    [getCategory.fulfilled]: (state, action) => {
      state.status = "sucess";
      state.category = action.payload;
    },
    [getCategory.rejected]: (state) => {
      state.status = "failed";
    },
  },
});

export default categorySlice.reducer;
