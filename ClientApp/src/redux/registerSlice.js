import { createSlice } from "@reduxjs/toolkit";

const registerSlice = createSlice({
  name: "register",
  initialState: {
    user: {
      userId: 2,
      userName: "registerSlice",
      email: "registerSlice@gmail.com",
      pasword: 12345,
    },
  },
  reducers: {
    login(state) {
      return;
    },
    logout(state) {
      state.user = {};
    },
  },
});

export const { logout } = registerSlice.actions;

export default registerSlice.reducer;
