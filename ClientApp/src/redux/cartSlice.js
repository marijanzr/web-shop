import { createSlice } from "@reduxjs/toolkit";

const cartSlice = createSlice({
  name: "cart",
  initialState: {
    cart: [],
    favorite: [],
    numberOfProductsInCart: 0,
    totalPriceOfProducts: 0,
  },
  reducers: {
    productsInCart(state) {
      return state.cart;
    },
    addProductInCart(state, action) {
      state.cart.push(action.payload);
    },
    addProductToFavorite(state, action) {
      state.favorite.push(action.payload);
    },
    removeProductFromFavorite(state, action) {
      state.favorite = state.favorite.filter(
        (product) => product.productId !== action.payload
      );
    },
    removeProductFromCart(state, action) {
      state.cart = state.cart.filter(
        (product) => product.productId !== action.payload
      );
    },
    incrementNumberOfProducts(state) {
      state.numberOfProductsInCart += 1;
    },
    decrementNumberOfProducts(state) {
      state.numberOfProductsInCart -= 1;
    },
    calculateTotalPrice(state) {
      state.totalPriceOfProducts = state.cart.reduce(
        (accumulator, current) => accumulator + current.price,
        0
      );
    },
  },
});
export const {
  productsInCart,
  addProductInCart,
  removeProductFromCart,
  numberOfProducts,
  incrementNumberOfProducts,
  decrementNumberOfProducts,
  addProductToFavorite,
  removeProductFromFavorite,
  calculateTotalPrice,
} = cartSlice.actions;
export default cartSlice.reducer;
