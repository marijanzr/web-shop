import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { API, API_POST } from "./indexApi";

export const getProducts = createAsyncThunk(
  "products/getProducts",
  async () => {
    return await fetch(`${API}/products`).then((res) => res.json());
  }
);

export const createProduct = (productPost) =>
  createAsyncThunk("products/createProduct", async () => {
    return await fetch(`${API}/products`, API_POST(productPost)).then((res) =>
      res.json()
    );
  });

const productSlice = createSlice({
  name: "products",
  initialState: {
    products: [],
    product: {},
    status: null,
    filteredProducts: [],
  },
  reducers: {
    getProduct(state, action) {
      return state.products.filter(
        (product) => product.productId !== action.payload
      );
    },
    filterProductByName(state, action) {
      if (!action.payload) {
        state.filteredProducts = state.products;
      } else {
        state.filteredProducts = state.products.filter((product) =>
          product.title.toLowerCase().includes(action.payload)
        );
      }
    },

    filterProductByCategory(state, action) {
      if (action.payload < 0) {
        state.filteredProducts = state.products;
      } else {
        state.filteredProducts = state.products.filter(
          (product) => product.categoryId == action.payload
        );
      }
    },
  },

  extraReducers: {
    [getProducts.pending]: (state) => {
      state.action = "loading";
    },
    [getProducts.fulfilled]: (state, action) => {
      state.status = "sucess";
      state.products = action.payload;
      state.filteredProducts = action.payload;
    },
    [getProducts.rejected]: (state) => {
      state.status = "failed";
    },

    [createProduct.pending]: (state) => {
      state.action = "loading";
    },
    [createProduct.fulfilled]: (state, action) => {
      state.status = "sucess";
      state.products.push(action.payload);
    },
    [createProduct.rejected]: (state) => {
      state.status = "failed";
    },
  },
});
export const { getProduct, filterProductByName, filterProductByCategory } =
  productSlice.actions;
export default productSlice.reducer;
