export const API = `https://localhost:5001/api`;

export const API_POST = (postObject = "defaultBody") => {
  return {
    method: "POST",
    body: JSON.stringify(postObject),
    headers: { "Content-type": "application/json" },
  };
};

export const createUser = (user) => {
  fetch(`${API}/users/register`, {
    method: "POST",
    body: JSON.stringify(user),
    headers: { "Content-type": "application/json" },
  });
};

export const createProduct = (product) => {
  fetch(`${API}/products`, {
    method: "POST",
    body: JSON.stringify(product),
    headers: { "Content-type": "application/json" },
  });
};
