import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { API, API_POST } from "./indexApi";

export const getToken = createAsyncThunk("users/login", async (user) => {
  return await fetch(`${API}/users/login`, API_POST(user)).then((res) => {
    return res.json();
  });
});

const loginSlice = createSlice({
  name: "user",
  initialState: {
    user: {
      email: "",
      password: "",
    },
    isLoggedIn: false,
    token: null,
    status: null,
  },

  reducers: {
    logOutUser(state) {
      state.isLoggedIn = false;
    },
  },

  extraReducers: {
    [getToken.pending]: (state) => {
      state.action = "loading";
    },
    [getToken.fulfilled]: (state, action) => {
      state.status = "sucess";
      state.token = action.payload;
      state.isLoggedIn = true;
    },
    [getToken.rejected]: (state) => {
      state.status = "failed";
    },
  },
});

export const { logOutUser } = loginSlice.actions;
export default loginSlice.reducer;
