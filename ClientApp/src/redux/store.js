import { configureStore } from "@reduxjs/toolkit";
import productReducer from "./productSlice";
import categoryReducer from "./categorySlice";
import cartReducer from "./cartSlice";
import registerSlice from "./registerSlice";
import loginSlice from "./loginSlice";

export default configureStore({
  reducer: {
    category: categoryReducer,
    products: productReducer,
    cart: cartReducer,
    userRegister: registerSlice,
    userLogin: loginSlice,
  },
});
