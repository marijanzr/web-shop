import React from "react";
import { Route } from "react-router";

import "./custom.css";

import Layout from "./components/Layout";
import Home from "./pages/Home";
import Contact from "./pages/Contact";
import ShoppingCart from "./pages/ShoppingCart";
import ProductDetails from "./components/ProductDetails";
import Login from "./pages/Login";
import Registration from "./pages/Registration";
import Admin from "./pages/Admin";
import Favorite from "./pages/Favorite";

function App() {
  return (
    <Layout>
      <Route exact path="/" component={Home} />
      <Route exact path="/contact" component={Contact} />
      <Route exact path="/shoppingcart" component={ShoppingCart} />
      <Route exact path="/products/:id" component={ProductDetails} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/registration" component={Registration} />
      <Route exact path="/favorite" component={Favorite} />
      <Route exact path="/admin" component={Admin} />
    </Layout>
  );
}

export default App;
